package rasa


type Intent struct {
   Name        string  `json:"name,omitempty"`
   Confidence  float64 `json:"confidence,omitempty"`
}
type Entity struct {
   Start       int     `json:"start,omitempty"`
   End         int     `json:"end,omitempty"`
   Value       string  `json:"value,omitempty"`
   Entity      string  `json:"entity,omitempty"`
   Confidence  float64 `json:"confidence,omitempty"`
   Extractor   string  `json:"extractor,omitempty"`
}
type IntentContainer struct {
   Intent      Intent  `json:"intent,omitempty"`
   Entities  []Entity  `json:"entities,omitempty"`
   Text        string  `json:"text,omitempty"`
}
